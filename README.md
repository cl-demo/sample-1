# sample-1

GitLabの練習プロジェクト

```math
\f{x} = \int_{-\infty}^\infty
    \hat \f\xi\,e^{2 \pi i \xi x}
    \,d\xi
```